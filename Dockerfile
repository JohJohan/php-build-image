ARG PHP_BUILD_IMAGE_MAJOR_PHP_VERSION
ARG PHP_BUILD_IMAGE_DEBIAN_CODENAME
ARG PHP_BUILD_IMAGE_SAPI
FROM php:${PHP_BUILD_IMAGE_MAJOR_PHP_VERSION}-${PHP_BUILD_IMAGE_SAPI}-${PHP_BUILD_IMAGE_DEBIAN_CODENAME}
SHELL ["/bin/bash", "-c"]

ARG PHP_BUILD_IMAGE_MAJOR_PHP_VERSION
WORKDIR /root
COPY extensions/php-ext-brotli /root/php-ext-brotli
COPY ci/docker_install_php.sh .
COPY ci/memory-limit-php.ini "/usr/local/etc/php/conf.d/memory-limit-php.ini"
ENV PHP_BUILD_IMAGE_MAJOR_PHP_VERSION=${PHP_BUILD_IMAGE_MAJOR_PHP_VERSION}
RUN ./docker_install_php.sh
