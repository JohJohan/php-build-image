# PHP Build Image

PHP cli images with most of the php modules pre-installed as well as git and composer.
To be used for pipeline testing and build jobs.

## Supported versions
The following php versions are currently supported:
 - 7.3
 - 7.4
 - 8.0
 - 8.1
 - 8.2
